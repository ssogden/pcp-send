#!/bin/bash

################### Config Vars ###################
#__serviceArray=("sshd" "nosuchd")
#__apiKey="changeme"
#__orgSeverity="3"
#__objOwner="support"
#__objAttr="default"
#__objAlarm="myalarm"
#__besDest="http://bes.domain.tld"

source wd_service
__date=$(date -u '+%Y/%m/%d %H:%M:%S')
__basename=$(basename $BASH_SOURCE)
__arr=()
################### End Config Vars ###################

alarm () {

   curl \
   --connect-timeout 5 \
   --max-time 10 \
   --retry 3 \
   --retry-delay 0 \
   --retry-max-time 10 \
   -H "Content-Type: application/json" \
   -H "Accept: application/json" \
   -d "{\"apiKey\":\"${__apiKey}\", \"AlarmName\": \"${__objAlarm}\", \"objectowner\": \"${__objOwner}\", \"domain\": \"$HOSTNAME\", \"origindatetime\": \"${__date}\", \"originseverity\": \"${__orgSeverity}\", \"Description\": \"$1 not running\"}" \
   -X POST "${__besDest}" \
   -i

    __status=$?
    [ ${__status} -eq 0 ] && 2>/dev/null || logger -s "${__basename}: POST request to ${__besDest} has failed to execute"

}


while getopts ":m:t:p:a:o:" opt; do # I've found for each new arg need to add 'X:' i.e. the letter and ':'
  case ${opt} in
    m )
      __arr+=("${OPTARG}")
      ;;
    t )
      __objOwner="${OPTARG}"
      ;;
    p )
      __orgSeverity="${OPTARG}"
      ;;
    a )
      __objAlarm="${OPTARG}"
      ;;
    o )
      __objAttr="${OPTARG}"
      ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument" 1>&2
      ;;
  esac
done
shift $((OPTIND -1))


for i in "${__arr[@]}"; do
    alarm "${__serviceArray[$i]}" 
done

exit 0
